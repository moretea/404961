# What works
MyApp.config do
  self.jar "miglayout-3.7"
  self.gem "active_resource"
end


# What i'd like to see
MyApp.config do
  jar "miglayout-3.7"
  gem "active_resource"
end


# How I call this block:
    def config &block
      config = BoogieWoogie::Application::Config.new(self)
      config.instance_exec &block
    end
